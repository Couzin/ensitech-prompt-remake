passw() {
  echo -n "New Password: " && read password
  echo && echo -n "Password will be changed, do you confirm ? [Y/n]" && read confirm

  if [ $confirm = "Y" ]; then
    # delete old password line
    command sed -i '/^password/d' ./${username}.txt 
    echo "password:${password}" >> ./${username}.txt
    echo "Password changed"
  elif [ $confirm = "n" ]; then
    echo "Password not changed"
  else
    echo "Please type Y or n"
  fi
}
