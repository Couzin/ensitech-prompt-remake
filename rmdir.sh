rmdir() {
  echo ${argv[1]}
  if [ -d ${argv[1]} ]; then
    command rm -r ${argv[1]}
  else
    echo "No such directory"
  fi
}