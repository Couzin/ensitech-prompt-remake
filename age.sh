age() {
  echo -n "Enter your age: " && read age
  if [ $age -lt 0 ]; then
    echo "You are not born yet dude"
  elif [ $age -ge 18 ]; then
    echo "You are an adult"
  else
    echo "You are a child"
  fi
}