# Prompt REMAKE

This project is about to recreate bash commands in a prompt.

## Getting Started

Run `main.sh` in terminal then create your credentials.  
**Note**: If it is the first connection, just type the username and password you want to create, then let you guide.  
After that u should be able to use the prompt.


## Available commands

- quit: leave the prompt
- help: display help
- about: short description of the project
- version: display the version of the project
- about: short sentence of description
- age: ask your age and display if you are adult or child
- ls: replica of standard ls command
- rm: replica of standard rm command
- rmdir: given directory name, remove it
- profil: display created profile infos
- passw: change password
- cd: replica of standard cd command
- pwd: replica of standard pwd command
- hour: display current hour to format HH:MM
- httpget: given url, put url source code in a file
- smtp: send an email via curl. Works with google mail only. You have to create a google app_password and put it in a .env file.
- open: open a file with vim.
  
**Note**: Maybe you'll have to change shebang on main.sh to your bash path. `which bash` to find the path.
**Note2**: Mutliple account is not implemented yet. You can create only one account.
