help() {
  # be sure that command and description match on lists
  commandName=(
    "\033[1mhelp | --help | -h\033[0m"
    "\033[1mquit | exit\033[0m"
    "\033[1mabout\033[0m"
    "\033[1mversion | vers | -v\033[0m"
    "\033[1mage\033[0m"
    "\033[1mls\033[0m"
    "\033[1mrm\033[0m"
    "\033[1mrmdir\033[0m"
    "\033[1mprofil\033[0m"
    "\033[1mpassw\033[0m"
    "\033[1mcd\033[0m"
    "\033[1mpwd\033[0m"
    "\033[1mhour\033[0m"
    "\033[1mhttpget\033[0m"
    "\033[1msmtp\033[0m"
    "\033[1mopen\033[0m"
  )

  commandDescription=(
    "Display this help text."
    "Exit the program."
    "Display information about this program."
    "Display the version of this program."
    "Ask user for age and return if adult or not."
    "List files, uses options of standard ls."
    "Remove files, uses options of standard rm."
    "Remove specified directory."
    "Display current profile information."
    "Change password of current profile."
    "Change current directory."
    "Display current directory."
    "Display current hour to format HH:MM."
    "Put source code of a web page in a file. url is typed after httpget. e.g. httpget https://www.google.com"
    "Send an email. You will be asked for destination email, subject and message."
    "Open a file with vim. Add the file name after open. e.g. open file.txt"
  )

  for (( i = 0; i < ${#commandName[@]}; i++ )); do
    echo -e "  ${commandName[$i]} - ${commandDescription[$i]}"
  done

  echo ""
}
