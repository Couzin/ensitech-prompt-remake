#!/bin/bash
source quit.sh
source help.sh
source about.sh
source version.sh
source age.sh
source ls.sh
source rm.sh
source rmdir.sh
source profil.sh
source passw.sh
source cd.sh
source pwd.sh
source hour.sh
source httpget.sh
source smtp.sh
source open.sh

cmd() {
  cmd=$1
  argv=$@

  case "${cmd}" in
    quit | exit ) quit;;
    help | --help | -h ) help;;
    about ) about;;
    version | vers | -v ) version;;
    age ) age;;
    ls ) ls;;
    rm ) rm;;
    rmdir ) rmdir;;
    profil ) profil;;
    passw ) passw;;
    cd ) cd;;
    pwd ) pwd;;
    hour ) hour;;
    httpget ) httpget;;
    smtp ) smtp;;
    open ) open;;
    * ) echo "Command not found";;
  esac
}

createProfile() {
  echo -n "create email: " && read email
  echo -n "create first name: " && read firstName
  echo -n "create last name: " && read lastName
  echo -n "create age: " && read age

  echo "username:${username}" > ./${username}.txt
  echo "password:${password}" >> ./${username}.txt
  echo "email:${email}" >> ./${username}.txt
  echo "firstName:${firstName}" >> ./${username}.txt
  echo "lastName:${lastName}" >> ./${username}.txt
  echo "age:${age}" >> ./${username}.txt
}

checkProfileValue() {
  if [ $(cat ./${username}.txt | grep "${1}") ]; then
    return 0
  else
    return 1
  fi
} 

login() {
  echo -n "Enter your username: " && read username
  echo -n "Enter your password: " && read -s password
    if [ -e ./${username}.txt ]; then
      $(checkProfileValue "username:${username}")
      checkUser=$?
      $(checkProfileValue "password:${password}")
      checkPassword=$?
      if [ ${checkUser} -eq 0 ] && [ ${checkPassword} -eq 0 ];
      then
        echo && echo "welcome ${username}"
      else
        echo && echo "bad credentials"
        login
      fi
    else
      echo "You didn't have a profile yet. Those credentials will be used to create your profile."
      createProfile
    fi
}

main() {
  login
  lineCount=1

  while [ 1 ]; do
    
    echo -e "$(hour) - [\033[31m${lineCount}\033[m] - \033[33m${username}\033[m ~ ☠️ ~ $(command pwd)" 
    read string

    cmd $string 
    lineCount=$(($lineCount+1))
  done
}

main