smtp() {
  echo && echo -n "Enter destination email: " && read to
  echo -n "Enter subject: " && read subject
  echo -n "Enter message: " && read message

  # getting existing datas
  from=$(cat ./${username}.txt | grep "email" | cut -d':' -f2)
  app_password=$(cat ./.env)

  # formatting mail
  echo "From: ${from}" > ./mail.txt
  echo "To: ${to}" >> ./mail.txt
  echo "Subject: ${subject}" >> ./mail.txt
  echo $message >> ./mail.txt

  # sending mail
  command curl --ssl-reqd \
    --url "smtps://smtp.gmail.com:465" \
    --user "${from}:${app_password}" \
    --mail-from "${from}" \
    --mail-rcpt "${to}" \
    --upload-file ./mail.txt

  # deleting mail file
  command rm ./mail.txt
}